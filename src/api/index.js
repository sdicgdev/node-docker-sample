#!/usr/bin/env node

var express = require('express');
var app = express();
var config = require('../../etc/database-connections.json');
var db = require('mysql-promise')();
var bodyParser = require('body-parser');

db.configure(config);

// parses requests as json
app.use(bodyParser.json());

app.get('/', function(req, res){
  db.query("SELECT * FROM users")
    .spread(function(rows){
      res.send(JSON.stringify(rows));
    });
});

app.listen(3000);
