Docker Overview
===============

There are two ways that docker is typically used. The first is with
Dockerfiles. Dockerfiles define individual containers, and use a particular
syntax to set up each container to deal with the application it is intended
to support.

The second is with a docker-compose file. docker-compose may still take
advantage of Dockerfiles. In this way, it is possible to define unique
containers, but start them in an orchestrated way. This makes the process
of starting each application much easier in a multi-app docker setup

before reading this file, you should take a look at docker-compose.yml 
in the root of this repository to see its general format

docker-compose.yml
=================

There are no Dockerfiles in this project. When an image is used, whether in
a Dockerfile or in a container defined in docker-compose.yml, it will typically
reference another Dockerfile. In most cases, these offer a way to run code
without doing extra things, provided no extra packages are required. For example,
in a PHP setup it would likely be required to install mysql bindings. In this case,
however, there is actually no reason to set up any container further than nginx,
node, and mysql have set up their containers to work by default

There are three boxes defined in docker-compose.yml. All of them follow the 
same basic format. More information on the docker-compose.yml options is available 

https://docs.docker.com/compose/compose-file/

In this docker-compose example, there are only a couple of options used. We will
discuss the following definition, taken from the docker-compose.yml file in this project


    nginx:
      image: nginx
      ports:
        - "80:80"
        - "443:443"
      volumes:
        - ./etc/nginx/nginx.conf:/etc/nginx/nginx.conf
        - ./src:/etc/nginx/html
      links:
        - node

The first line is the reference name of the container. This name is used in the links
section of other containers. More on that in a moment. 

The first option under the reference name is `image`. The `image` flag indicates the 
base image that the container should be built from. nginx, in this case, refers to [the 
nginx container](https://hub.docker.com/_/nginx/) 

Next is ports. Ports are defined as an array of "host:container". Ports only need to be
defined for outside access. Docker containers across a docker-compose install have access
to one another. This is a benefit of using docker-compose, as internal ports do need to 
be exposed when containers are started one at a time.

Next is volumes, which allows files and directories to be mapped from the development machine
to the container. One file may be mapped to more than one place. In this case, the `src/` 
directory is pointed to the place that nginx expects to host its source from. 

Finally, links. Links allows internal networking from one container to another. If we look
at the nginx config in etc/, we will see a reference to this link 


      upstream api_service {
        server node:3000;
      }

node:3000, in this case, refers to the node container on port 3000. Docker edits each containers
host file according to the links defined. You can also see this in effect in the
`etc/database-containers.json`.

MySQL
=====

In order to load sql into the container, we define a volume in the mysql container that points
to an initialization directory used with mysql. Any .sql file in var/mysql will be run automatically
when the containers are started.

It is still required to set the mysql root password and the working database, and these are taken
care of in an `environments` key. Information in this section is put into the environment variables
of the container being defined.


DOCKER REFERENCE
================

see the following reference for some common `docker-compose` commands. `docker-compose help` is also useful

| command | purpose | usage |
| ------------ | ------------ | ---------|
| up | initializes the containers and starts them with the logs. this must be done once. ctrl-c stops the services. Can be run with the `-d` argument to start in the background| `docker-compose up` |
| start | start the containers defined by docker-compose.yml | `docker-compose start` |
| stop | stop the running containers |  `docker-compose stop` |
| logs | view the logs of the current running containers |  `docker-compose logs` |
| run | execute a command on a running container. e.g. `docker-compose run php bash` |  `docker-compose run` |
| rm | After stopping via `docker-compose stop` the container can be removed. You will have to `docker-compose build` before you `docker-compose up` when you use this command | `docker-compose rm -f` |

Finally, to get onto the command line of one of the containers:

    docker-compose run nginx bash

in this case, we are requesting to run the command `bash` on the container labeled `nginx`.
It is possible to run other commands, but bash opens a prompt, allowing for inspection of the container